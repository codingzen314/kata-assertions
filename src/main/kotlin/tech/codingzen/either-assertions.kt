package tech.codingzen

import tech.codingzen.kata.either.*
import kotlin.test.*

fun assertIsLeft(actual: Either<*, *>): Unit {
  assertTrue(actual.isLeft(),"expected to be Left")
}

fun assertIsNotLeft(actual: Either<*, *>): Unit {
  assertTrue(!actual.isLeft(), "expected to not be Left")
}

fun assertIsRight(actual: Either<*, *>): Unit {
  assertTrue(actual.isRight(), "expected to be Right")
}

fun assertIsNotRight(actual: Either<*, *>): Unit {
  assertTrue(!actual.isRight(), "expected to not be Right")
}

fun <L> assertLeft(expected: L, actual: Either<L, *>): Unit {
  when (actual) {
    is Left -> assertEquals( expected, actual.left, "expected a left to be equal to $expected")
    is Right -> fail("expected a left to be equal to $expected")
  }
}

fun <R> assertRight(expected: R, actual: Either<*, R>): Unit {
  when (actual) {
    is Left -> fail("expected a left to be equal to $expected")
    is Right -> assertEquals(expected, actual.right, "expected a left to be equal to $expected")
  }
}