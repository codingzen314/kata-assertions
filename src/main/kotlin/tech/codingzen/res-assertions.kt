package tech.codingzen

import tech.codingzen.kata.result.Err
import tech.codingzen.kata.result.Ok
import tech.codingzen.kata.result.Res
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

fun assertIsErr(actual: Res<*>): Unit {
  assertTrue(actual is Err,"expected to be Err")
}

fun assertIsNotErr(actual: Res<*>): Unit {
  assertTrue(actual !is Err, "expected to not be Err")
}

fun assertIsOk(actual: Res<*>): Unit {
  assertTrue(actual is Ok, "expected to be Ok")
}

fun assertIsNotOk(actual: Res<*>): Unit {
  assertTrue(actual !is Ok, "expected to not be Ok")
}

fun assertErr(expected: Err, actual: Res<*>): Unit {
  when (actual) {
    is Err -> assertEquals(expected, actual, "expected an Err to be equal to $expected")
    is Ok -> fail("expected an Err to be equal to $expected")
  }
}

fun <R> assertOk(expected: R, actual: Res<R>): Unit {
  when (actual) {
    is Err -> fail("expected an Ok to be equal to $expected")
    is Ok -> assertEquals(expected, actual.value, "expected an Ok to be equal to $expected")
  }
}