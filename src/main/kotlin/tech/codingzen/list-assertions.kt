package tech.codingzen

import tech.codingzen.kata.list.KataList
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

fun assertIsEmpty(list: KataList<*>) {
  assertTrue(list.isEmpty(), "Expected KataList to be empty")
}

fun assertIsNil(list: KataList<*>) {
  assertTrue(list.isNil, "Expected KataList to be nil")
}

fun assertIsNotNil(list: KataList<*>) {
  assertFalse(!list.isNil, "Expected KataList to not be nil")
}

fun assertIsCons(list: KataList<*>) {
  assertTrue(list.isCons, "Expected KataList to be nil")
}

fun assertIsNotCons(list: KataList<*>) {
  assertFalse(!list.isCons, "Expected KataList to not be nil")
}

fun assertIsNotEmpty(list: KataList<*>) {
  assertFalse(list.isEmpty(), "Expected KataList to not be empty")
}

fun assertSize(expected: Int, list: KataList<*>) {
  assertEquals(expected, list.size, "Expected KataList to have size of $expected")
}

fun <T> assertHead(expected: T, list: KataList<T>) {
  assertEquals(expected, list.head, "Expected head of KataList to be $expected")
}

fun <T> assertTail(expected: KataList<T>, list: KataList<T>) {
  assertEquals(expected, list.tail, "Expected head of KataList to be $expected")
}

fun <T> assertIndexOf(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOf(value), "Expected index of $value to be $expected")
}

fun <T> assertIndexOfFirst(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOfFirst(value), "Expected index of $value to be $expected")
}

fun <T> assertIndexOfLast(expected: Int, value: T, list: KataList<T>) {
  assertEquals(expected, list.indexOfLast(value), "Expected index of $value to be $expected")
}